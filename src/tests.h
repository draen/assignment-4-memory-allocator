#ifndef TESTS_H
#define TESTS_H

#include <stdbool.h>
#include <stdint.h>
#define __USE_MISC 1

#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

typedef bool (*test_func)();

extern test_func testers[];

void for_each_test(void (*action)(test_func, size_t testNum));

#endif