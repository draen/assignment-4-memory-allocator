#include "tests.h"
#include "tests_utils.h"

#define DEBUG_FILE stderr

extern struct block_header* get_header_from_contents(void* contents);
extern void alloc_bunch(void* items[], size_t len);
extern void check_bunch(void* items[], size_t len, bool* res);
extern void free_bunch(void* items[], size_t len);

//malloc for 1 item
static bool test0() {
    void* heap = heap_init(16384);
        debug_heap(DEBUG_FILE, heap);
    void* attempt = _malloc(8192);

    debug_heap(DEBUG_FILE, heap);

    if (heap==NULL || attempt==NULL) {
        return false;
    } 

    _free(attempt);

    debug_heap(DEBUG_FILE, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 16384}).bytes);
    return true;
}

//malloc a bunch of items
static bool test1() {
    void* heap = heap_init(16384);

    size_t arrLen = 10;
    void* items[arrLen];
    alloc_bunch(items, arrLen);

    debug_heap(DEBUG_FILE, heap);

    bool res = heap!=NULL;
    check_bunch(items, arrLen, &res);

    if (!res) return false;

    free_bunch(items, arrLen);

    debug_heap(DEBUG_FILE, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 16384}).bytes);
    return true;
}

//malloc a bunch, then free all of it and reuse one of the blocks (while merging some before it)
static bool test2() {
    void* heap = heap_init(16384);

    size_t arrLen = 5;
    void* items1[arrLen];
    alloc_bunch(items1, arrLen);

    debug_heap(DEBUG_FILE, heap);

    bool res = heap!=NULL;
    check_bunch(items1, arrLen, &res);

    if (!res) return false;

    free_bunch(items1, arrLen);

    void* newItem = _malloc(1024);
    debug_heap(DEBUG_FILE, heap);

    if (newItem==NULL) return false;

    struct block_header* block = get_header_from_contents(newItem);
    if (block->capacity.bytes!=1024) return false;

    struct block_header* nextBlock = block->next;
    if (nextBlock->next !=NULL) return false;


    _free(newItem);

    debug_heap(DEBUG_FILE, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 16384}).bytes);
    return true;
}

//try to allocate more than heap size
static bool test3() {
    //init MIN_SIZE
    void* heap = heap_init(1);
    debug_heap(DEBUG_FILE, heap);
    void* item = _malloc(REGION_MIN_SIZE+1024);
    debug_heap(DEBUG_FILE, heap);

    struct block_header* block = (struct block_header *) heap;
    if (block->capacity.bytes < REGION_MIN_SIZE+1024) return false;

    _free(item);
    debug_heap(DEBUG_FILE, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 20463}).bytes);
    return true;
}

//try to allocate more than heap size, but regions won't be next to each other
static bool test4() {
    //init MIN_SIZE
    void* heap = heap_init(1);
    debug_heap(DEBUG_FILE, heap);

    (void)mmap((heap+REGION_MIN_SIZE), 1024, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0);

    void* item = _malloc(REGION_MIN_SIZE+1024);
    debug_heap(DEBUG_FILE, heap);

    struct block_header * firstBlock = (struct block_header *) heap;
    if (! firstBlock->is_free) return false;

    struct block_header * next = firstBlock->next;

    _free(item);
    debug_heap(DEBUG_FILE, heap);

    munmap(heap, REGION_MIN_SIZE);
    munmap(next, size_from_capacity((block_capacity){.bytes = 12271}).bytes);
    return  true;
}

test_func testers[] = {
    test0,
    test1,
    test2,
    test3,
    test4
};

void for_each_test(void (*action)(test_func, size_t test_num)) {
    size_t len = sizeof(testers) / sizeof(testers[0]);
    for (size_t i = 0; i < len; i++) {
        action(testers[i], i);
    }
}