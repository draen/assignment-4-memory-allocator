#include "tests.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define DEBUG_FILE stderr

void handle_test(test_func test, size_t testNum) {
    fprintf(stderr,"\n--------- Test #%" PRIu64 " ---------\n", testNum);
    bool res = test();
    if (!res) {
        fprintf(stderr,"test #%" PRIu64 " failed\n", testNum);
        abort();
    }
    fprintf(stderr,"--------- Test #%" PRIu64 " passed ---------\n", testNum);
}

int main() {
    for_each_test(handle_test);
}


