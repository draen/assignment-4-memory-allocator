#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  //get actual region size
  size_t trueSize = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
  //try to map region immediately after heap
  void* resAddr = map_pages(addr, trueSize, MAP_FIXED_NOREPLACE);
  //if that failed, try to map it elsewhere
  if  (resAddr==MAP_FAILED) {
    resAddr = map_pages(addr, trueSize, 0);
  }
  //if that failed too, initialize invalid region
  void* trueAddr = resAddr==MAP_FAILED ? NULL : resAddr;
  struct region region = {.addr = trueAddr, .extends = trueAddr==addr, .size = trueSize};
  //if region is valid, initialize block
  if (! region_is_invalid(&region)) {
    block_init(trueAddr, (block_size) {.bytes = region.size}, NULL);
  }
  return region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  //if can't split - fail
  if (! block_splittable(block, query)) return false;
  //addr of splitted block
  void* addr = block->contents+query;
  //create splitted block
  block_init(addr, (block_size) {.bytes = block->capacity.bytes - query}, block->next);
  //update initial block
  block->capacity.bytes = query;
  block->next = addr;
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header * next = block->next;
  //if no next block or not mergeable - fail
  if (next==NULL || ! mergeable(block, next)) return false;
  //update next block
  block->next = next->next;
  //increase capacity
  block->capacity.bytes += size_from_capacity(next->capacity).bytes;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  //unless block is null - iterate
  while (block) {
    //merge until it isn't possible
    while (try_merge_with_next(block)) {}
    //if resulting block is free and big enough - return it
    if (block->is_free && block_is_big_enough(sz, block)) {
      return (struct block_search_result) {.block = block, .type = BSR_FOUND_GOOD_BLOCK};
    }
    if (block->next == block) return (struct block_search_result) {.block = block, .type = BSR_CORRUPTED};
    if (block->next == NULL) break;
    //go to next block
    block = block->next;
  }
  //fail
  return (struct block_search_result) {.block = block, .type = BSR_REACHED_END_NOT_FOUND};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  //try to find applicable block
  struct block_search_result res = find_good_or_last(block, query);
  //if failed - return fail
  if (res.type!=BSR_FOUND_GOOD_BLOCK) return res;
  //otherwise try to split block and return it
  split_if_too_big(res.block, query);
  res.block->is_free = false;
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (last==NULL) return NULL;
  //allocate region
  struct region region = alloc_region(last->contents + last->capacity.bytes, query);
  //if last block is free & region is directly after heap - merge
  if (last->is_free && ! region_is_invalid(&region) && region.extends) {
    last->capacity.bytes += region.size;
    return last;
  }
  last->next = region.addr;
  return region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  //get actual query size
  query = size_max(query, BLOCK_MIN_CAPACITY);
  //try to alllocate with existing heap
  struct block_search_result res = try_memalloc_existing(query, heap_start);
  //if we failed by reaching end - try to grow heap
  if (res.type == BSR_REACHED_END_NOT_FOUND) {
    res.block = grow_heap(res.block, query);
    //if couldn't grow - return NULL
    if (res.block == NULL) return NULL;
    res = try_memalloc_existing(query, res.block);
  }
  //if heap is corrupted - return NULL
  if (res.type == BSR_CORRUPTED) return NULL;
  //if everything is good return block
  return res.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  //try to merge freed block with next ones for as long as we can
  while (try_merge_with_next(header)) {};
}
