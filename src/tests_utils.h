#ifndef TESTS_UTILS_H
#define TESTS_UTILS_H

#include "mem.h"


inline struct block_header* get_header_from_contents(void* contents) {
    return (struct block_header*) ((uint8_t*) contents - offsetof(struct block_header, contents));
}

inline void alloc_bunch(void* items[], size_t len) {
    for (size_t i = 0; i < len; i++) {
        items[i] = _malloc(512);
    }
}

inline void check_bunch(void* items[], size_t len, bool* res) {
    for (size_t i = 0; i < len; i++) {
        if (items[i]==NULL) *res = false;
        break;
    }
}

inline void free_bunch(void* items[], size_t len) {
    for (size_t i = 0; i < len; i++) {
        _free(items[i]);
    }
}

#endif